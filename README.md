# GMme

## About

This extension was created to make it look like you have the GM Title on Chess.com.
This project will only show on your local machine.
It is made for me to learn and is still in early development.

## Getting started

Open the 'Extensions' page in Chrome. Select 'Developer mode' in the top right. Click 'Load unpacked'. Select the folder 'GMme'. Reload Chess.com.

## Legal

This project was created purely to have fun with making extensions and not to mislead anyone into thinking you are a Grandmaster on Chess.com. If you choose to use this extension I am not responsible for any repurcussions such as being banned from Chess.com, any glitches with Chrome, or any other consequences related to the extension. There may be many glitches/bugs.


